/*
 * TarantulaMod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.spidermod.common;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.Identifier;

public class SpiderMod implements ModInitializer {

    public static final String MODID = "spidermod";

    public static final String TARANTULA_NAME = "tarantula";
    public static final String TARANTULA_SPAWN_EGG_NAME = "tarantula_spawn_egg";

    public static final String JUMPER_NAME = "jumper";
    public static final String JUMPER_SPAWN_EGG_NAME = "jumper_spawn_egg";

    public static final String WIDOW_NAME = "widow";
    public static final String WIDOW_SPAWN_EGG_NAME = "widow_spawn_egg";

    public static final String HUNTSMAN_NAME = "huntsman";
    public static final String HUNTSMAN_SPAWN_EGG_NAME = "huntsman_spawn_egg";

    public static final String GREEN_LYNX_NAME = "green_lynx";
    public static final String GREEN_LYNX_SPAWN_EGG_NAME = "green_lynx_spawn_egg";

    public static final EntityType<TarantulaEntity> TARANTULA = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(SpiderMod.MODID, TARANTULA_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, TarantulaEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<JumperEntity> JUMPER = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(SpiderMod.MODID, JUMPER_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, JumperEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<BlackWidowEntity> WIDOW = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(SpiderMod.MODID, WIDOW_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, BlackWidowEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<HuntsmanEntity> HUNTSMAN = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(SpiderMod.MODID, HUNTSMAN_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, HuntsmanEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    public static final EntityType<GreenLynxEntity> GREEN_LYNX = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(SpiderMod.MODID, GREEN_LYNX_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, GreenLynxEntity::new).dimensions(EntityDimensions.fixed(0.25f, 0.25f)).trackedUpdateRate(3).trackRangeBlocks(32).build()
    );

    @Override
    public void onInitialize() {
        FabricDefaultAttributeRegistry.register(TARANTULA, TarantulaEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(JUMPER, JumperEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(WIDOW, BlackWidowEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(HUNTSMAN, HuntsmanEntity.createAttributes());
        FabricDefaultAttributeRegistry.register(GREEN_LYNX, GreenLynxEntity.createAttributes());

        Registry.register(Registry.ITEM, new Identifier(SpiderMod.MODID, TARANTULA_SPAWN_EGG_NAME), new SpawnEggItem(TARANTULA, 0x8B2323, 0x0000CD, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(SpiderMod.MODID, JUMPER_SPAWN_EGG_NAME), new SpawnEggItem(JUMPER, 0x483D8B, 0xDC143C, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(SpiderMod.MODID, WIDOW_SPAWN_EGG_NAME), new SpawnEggItem(WIDOW, 0x292929, 0xDC143C, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(SpiderMod.MODID, HUNTSMAN_SPAWN_EGG_NAME), new SpawnEggItem(HUNTSMAN, 0x8B4513, 0xA52A2A, new Item.Settings().group(ItemGroup.MISC)));
        Registry.register(Registry.ITEM, new Identifier(SpiderMod.MODID, GREEN_LYNX_SPAWN_EGG_NAME), new SpawnEggItem(GREEN_LYNX, 0x32cc22, 0x29ff29, new Item.Settings().group(ItemGroup.MISC)));
    }
}
