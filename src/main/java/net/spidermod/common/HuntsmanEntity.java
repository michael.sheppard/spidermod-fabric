/*
 * HuntsmanEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.spidermod.common;


import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.ai.pathing.EntityNavigation;
import net.minecraft.entity.ai.pathing.SpiderNavigation;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.BeeEntity;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.*;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

public class HuntsmanEntity extends AnimalEntity {
    private static final TrackedData<Byte> SPIDER_FLAGS;
    private static final float MAX_SIZE = 0.30f;
    private static final float MIN_SIZE = 0.25f;

    private final float WIDTH = 3.0f;
    private final float HEIGHT = 1.0f;
    private final float scaleFactor;

    public EntityDimensions tarantulaSize = new EntityDimensions(WIDTH, HEIGHT, false);

    public HuntsmanEntity(EntityType<? extends HuntsmanEntity> entity, World world) {
        super(entity, world);

        float scale = random.nextFloat();
        scaleFactor = Math.max(MIN_SIZE, Math.min(scale, MAX_SIZE));
        setHealth(16);
        tarantulaSize.scaled(WIDTH * scaleFactor, HEIGHT * scaleFactor);
    }

    public static DefaultAttributeContainer.Builder createAttributes() {
        return MobEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 30.0D)
                .add(EntityAttributes.GENERIC_FOLLOW_RANGE, 20.0D)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.25D)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 6.0D);
    }

    // implementing Biome specific spawning here
    @SuppressWarnings("unused")
    public static boolean canSpawn(EntityType<HuntsmanEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random) {
        return validSpawnBiomes(world, pos);
    }

    private static boolean validSpawnBiomes(WorldAccess world, BlockPos pos) {
        Optional<RegistryKey<Biome>> optional = world.getBiomeKey(pos);
        return Objects.equals(optional, Optional.of(BiomeKeys.FLOWER_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.PLAINS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BIRCH_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.JUNGLE)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SAVANNA));
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    @Override
    protected void initGoals() {
        super.initGoals();
        goalSelector.add(1, new SwimGoal(this));
        goalSelector.add(3, new PounceAtTargetGoal(this, 0.4F));
        goalSelector.add(4, new HuntsmanEntity.AttackGoal(this));
        goalSelector.add(5, new WanderAroundFarGoal(this, 0.8D));
        goalSelector.add(6, new LookAtEntityGoal(this, PlayerEntity.class, 8.0F));
        goalSelector.add(6, new LookAroundGoal(this));
        targetSelector.add(1, new RevengeGoal(this));
        targetSelector.add(2, new ActiveTargetGoal<>(this, BeeEntity.class, 10, true, true, null));
    }

    public double getMountedHeightOffset() {
        return this.getHeight() * 0.5F;
    }

    protected EntityNavigation createNavigation(World world) {
        return new SpiderNavigation(this, world);
    }

    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(SPIDER_FLAGS, (byte) 0);
    }

    public void tick() {
        super.tick();
        if (!this.world.isClient) {
            this.setClimbingWall(this.horizontalCollision);
        }
    }

    protected SoundEvent getAmbientSound() {
        return SoundEvents.ENTITY_SPIDER_AMBIENT;
    }

    protected SoundEvent getHurtSound(DamageSource source) {
        return SoundEvents.ENTITY_SPIDER_HURT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_SPIDER_DEATH;
    }

    protected void playStepSound(BlockPos pos, BlockState state) {
        this.playSound(SoundEvents.ENTITY_SPIDER_STEP, 0.15F, 1.0F);
    }

    public boolean isClimbing() {
        return this.isClimbingWall();
    }

    public void slowMovement(BlockState state, Vec3d multiplier) {
        if (!state.isOf(Blocks.COBWEB)) {
            super.slowMovement(state, multiplier);
        }

    }

    public EntityGroup getGroup() {
        return EntityGroup.ARTHROPOD;
    }

    public boolean canHaveStatusEffect(StatusEffectInstance effect) {
        return effect.getEffectType() != StatusEffects.POISON && super.canHaveStatusEffect(effect);
    }

    public boolean isClimbingWall() {
        return (dataTracker.get(SPIDER_FLAGS) & 1) != 0;
    }

    public void setClimbingWall(boolean climbing) {
        byte b = dataTracker.get(SPIDER_FLAGS);
        if (climbing) {
            b = (byte) (b | 1);
        } else {
            b &= -2;
        }

        this.dataTracker.set(SPIDER_FLAGS, b);
    }

    public EntityData initialize(ServerWorldAccess serverWorldAccess, LocalDifficulty difficulty, SpawnReason spawnReason, EntityData entityDataIn, NbtCompound entityTag) {
        EntityData entityData = super.initialize(serverWorldAccess, difficulty, spawnReason, entityDataIn, entityTag);
        if (entityData == null) {
            entityData = new HuntsmanEntity.SpiderData();
            if (serverWorldAccess.getDifficulty() == Difficulty.HARD && serverWorldAccess.getRandom().nextFloat() < 0.1F * difficulty.getClampedLocalDifficulty()) {
                ((HuntsmanEntity.SpiderData) entityData).setEffect(serverWorldAccess.getRandom());
            }
        }

        if (entityData instanceof HuntsmanEntity.SpiderData) {
            StatusEffect statusEffect = ((HuntsmanEntity.SpiderData) entityData).effect;
            if (statusEffect != null) {
                this.addStatusEffect(new StatusEffectInstance(statusEffect, Integer.MAX_VALUE));
            }
        }

        return entityData;
    }

    @Override
    public PassiveEntity createChild(ServerWorld serverWorld, PassiveEntity passiveEntity) {
        return SpiderMod.HUNTSMAN.create(serverWorld);
    }

    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return 0.65F;
    }

    static {
        SPIDER_FLAGS = DataTracker.registerData(HuntsmanEntity.class, TrackedDataHandlerRegistry.BYTE);
    }

//    static class FollowTargetGoal<T extends LivingEntity> extends net.minecraft.entity.ai.goal.FollowTargetGoal<T> {
//        public FollowTargetGoal(HuntsmanEntity spider, Class<T> targetEntityClass) {
//            super(spider, targetEntityClass, true);
//        }
//
//        public boolean canStart() {
//            float f = this.mob.getBrightnessAtEyes();
//            return !(f >= 0.5F) && super.canStart();
//        }
//    }

    static class AttackGoal extends MeleeAttackGoal {
        public AttackGoal(HuntsmanEntity spider) {
            super(spider, 1.0D, true);
        }

        public boolean canStart() {
            return super.canStart() && !this.mob.hasPassengers();
        }

        public boolean shouldContinue() {
            float f = this.mob.getBrightnessAtEyes();
            if (f >= 0.5F && this.mob.getRandom().nextInt(100) == 0) {
                this.mob.setTarget(null);
                return false;
            } else {
                return super.shouldContinue();
            }
        }

        protected double getSquaredMaxAttackDistance(LivingEntity entity) {
            return 4.0F + entity.getWidth();
        }
    }

    public static class SpiderData implements EntityData {
        public StatusEffect effect;

        public void setEffect(Random random) {
            int i = random.nextInt(5);
            if (i <= 1) {
                effect = StatusEffects.SPEED;
            } else if (i <= 2) {
                effect = StatusEffects.STRENGTH;
            } else if (i <= 3) {
                effect = StatusEffects.REGENERATION;
            } else {
                effect = StatusEffects.INVISIBILITY;
            }

        }
    }
}
