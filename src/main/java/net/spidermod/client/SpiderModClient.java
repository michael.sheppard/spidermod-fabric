/*
 * SpiderModClient.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.spidermod.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.minecraft.client.render.entity.model.EntityModelLayer;
import net.minecraft.util.Identifier;
import net.spidermod.common.SpiderMod;

public class SpiderModClient implements ClientModInitializer {
    public static final EntityModelLayer widowModelLayer = new EntityModelLayer(new Identifier(SpiderMod.MODID, "textures/entity/widow/widow.png"), SpiderMod.WIDOW_NAME);
    public static final EntityModelLayer tarantulaModelLayer = new EntityModelLayer(new Identifier(SpiderMod.MODID, "textures/entity/tarantula/tarantula.png"), SpiderMod.TARANTULA_NAME);
    public static final EntityModelLayer greenLynxModelLayer = new EntityModelLayer(new Identifier(SpiderMod.MODID, "textures/entity/green_lynx/green_lynx.png"), SpiderMod.GREEN_LYNX_NAME);
    public static final EntityModelLayer huntsmanModelLayer = new EntityModelLayer(new Identifier(SpiderMod.MODID, "textures/entity/huntsman/huntsman.png"), SpiderMod.HUNTSMAN_NAME);
    public static final EntityModelLayer jumperModelLayer = new EntityModelLayer(new Identifier(SpiderMod.MODID, "textures/entity/jumper/jumper.png"), SpiderMod.JUMPER_NAME);

    @Override
    public void onInitializeClient() {
        EntityModelLayerRegistry.registerModelLayer(widowModelLayer, BlackWidowModel::getTexturedModelData);
        EntityRendererRegistry.INSTANCE.register(SpiderMod.WIDOW, BlackWidowRenderer::new);

        EntityModelLayerRegistry.registerModelLayer(tarantulaModelLayer, TarantulaModel::getTexturedModelData);
        EntityRendererRegistry.INSTANCE.register(SpiderMod.TARANTULA, TarantulaRenderer::new);

        EntityModelLayerRegistry.registerModelLayer(greenLynxModelLayer, GreenLynxModel::getTexturedModelData);
        EntityRendererRegistry.INSTANCE.register(SpiderMod.GREEN_LYNX, GreenLynxRenderer::new);

        EntityModelLayerRegistry.registerModelLayer(huntsmanModelLayer, HuntsmanModel::getTexturedModelData);
        EntityRendererRegistry.INSTANCE.register(SpiderMod.HUNTSMAN, HuntsmanRenderer::new);

        EntityModelLayerRegistry.registerModelLayer(jumperModelLayer, JumperModel::getTexturedModelData);
        EntityRendererRegistry.INSTANCE.register(SpiderMod.JUMPER, JumperRenderer::new);
    }
}
