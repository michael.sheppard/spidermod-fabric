/*
 * Huntsman.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.spidermod.client;


import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.entity.feature.EyesFeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.entity.Entity;
import net.minecraft.util.Identifier;
import net.spidermod.common.SpiderMod;

@Environment(EnvType.CLIENT)
public class HuntsmanEyesLayer<T extends Entity, M extends HuntsmanModel<T>> extends EyesFeatureRenderer<T, M> {
    private static final RenderLayer EYES = RenderLayer.getEyes(new Identifier(
            SpiderMod.MODID, "textures/entity/huntsman_eyes.png"));

    public HuntsmanEyesLayer(FeatureRendererContext<T, M> rendererContext) {
        super(rendererContext);
    }

    @Override
    public RenderLayer getEyesTexture() {
        return EYES;
    }
}
