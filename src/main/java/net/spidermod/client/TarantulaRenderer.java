/*
 * TarantulaRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.spidermod.client;


import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.spidermod.common.SpiderMod;
import net.spidermod.common.TarantulaEntity;

@Environment(EnvType.CLIENT)
public class TarantulaRenderer<T extends TarantulaEntity> extends MobEntityRenderer<T, TarantulaModel<T>> {
    private static final Identifier TEXTURE = new Identifier(SpiderMod.MODID, "textures/entity/tarantula/tarantula.png");

    public TarantulaRenderer(EntityRendererFactory.Context context) {
        super(context, new TarantulaModel<>(context.getPart(SpiderModClient.tarantulaModelLayer)), 0.8F);
        addFeature(new TarantulaEyesLayer<>(this));
        shadowRadius *= 0.15f;
        shadowOpacity *= 0.5f;
    }

    protected void scale(T entity, MatrixStack matrixStack, float partialTickTime) {
        float scale = entity.getScaleFactor();
        matrixStack.scale(scale, scale, scale);
        super.scale(entity, matrixStack, partialTickTime);
    }

    protected float getLyingAngle(T spiderEntity) {
        return 180.0F;
    }

    public Identifier getTexture(T spiderEntity) {
        return TEXTURE;
    }
}