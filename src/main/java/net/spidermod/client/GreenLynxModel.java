/*
 * GreenLynxModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.spidermod.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.*;
import net.minecraft.client.render.entity.model.SinglePartEntityModel;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

@Environment(EnvType.CLIENT)
public class GreenLynxModel<T extends Entity> extends SinglePartEntityModel<T> {
    private final ModelPart root;
    private final ModelPart head;
    private final ModelPart rightHindLeg;
    private final ModelPart leftHindLeg;
    private final ModelPart rightMiddleLeg;
    private final ModelPart leftMiddleLeg;
    private final ModelPart rightMiddleFrontLeg;
    private final ModelPart leftMiddleFrontLeg;
    private final ModelPart rightFrontLeg;
    private final ModelPart leftFrontLeg;

    public GreenLynxModel(ModelPart root) {
        this.root = root;
        head = root.getChild("head");
        rightHindLeg = root.getChild("right_hind_leg");
        leftHindLeg = root.getChild("left_hind_leg");
        rightMiddleLeg = root.getChild("right_middle_hind_leg");
        leftMiddleLeg = root.getChild("left_middle_hind_leg");
        rightMiddleFrontLeg = root.getChild("right_middle_front_leg");
        leftMiddleFrontLeg = root.getChild("left_middle_front_leg");
        rightFrontLeg = root.getChild("right_front_leg");
        leftFrontLeg = root.getChild("left_front_leg");
    }

    public static TexturedModelData getTexturedModelData() {
        ModelData modelData = new ModelData();
        ModelPartData modelPartData = modelData.getRoot();
        modelPartData.addChild("head", ModelPartBuilder.create().uv(32, 4).cuboid(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 8.0F), ModelTransform.pivot(0.0F, 15.0F, -3.0F));
        modelPartData.addChild("body0", ModelPartBuilder.create().uv(0, 0).cuboid(-3.0F, -3.0F, -3.0F, 6.0F, 6.0F, 6.0F), ModelTransform.pivot(0.0F, 15.0F, 0.0F));
        modelPartData.addChild("body1", ModelPartBuilder.create().uv(0, 12).cuboid(-5.0F, -4.0F, -6.0F, 10.0F, 8.0F, 12.0F), ModelTransform.pivot(0.0F, 15.0F, 9.0F));
        ModelPartBuilder modelPartBuilder = ModelPartBuilder.create().uv(18, 0).cuboid(-15.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F);
        ModelPartBuilder modelPartBuilder2 = ModelPartBuilder.create().uv(18, 0).cuboid(-1.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F);
        modelPartData.addChild("right_hind_leg", modelPartBuilder, ModelTransform.pivot(-4.0F, 15.0F, 2.0F));
        modelPartData.addChild("left_hind_leg", modelPartBuilder2, ModelTransform.pivot(4.0F, 15.0F, 2.0F));
        modelPartData.addChild("right_middle_hind_leg", modelPartBuilder, ModelTransform.pivot(-4.0F, 15.0F, 1.0F));
        modelPartData.addChild("left_middle_hind_leg", modelPartBuilder2, ModelTransform.pivot(4.0F, 15.0F, 1.0F));
        modelPartData.addChild("right_middle_front_leg", modelPartBuilder, ModelTransform.pivot(-4.0F, 15.0F, 0.0F));
        modelPartData.addChild("left_middle_front_leg", modelPartBuilder2, ModelTransform.pivot(4.0F, 15.0F, 0.0F));
        modelPartData.addChild("right_front_leg", modelPartBuilder, ModelTransform.pivot(-4.0F, 15.0F, -1.0F));
        modelPartData.addChild("left_front_leg", modelPartBuilder2, ModelTransform.pivot(4.0F, 15.0F, -1.0F));
        return TexturedModelData.of(modelData, 64, 32);
    }

    public ModelPart getPart() {
        return root;
    }

    public void setAngles(T entity, float limbAngle, float limbDistance, float animationProgress, float headYaw, float headPitch) {
        head.yaw = headYaw * 0.017453292F;
        head.pitch = headPitch * 0.017453292F;
        rightHindLeg.roll = -0.7853982F;
        leftHindLeg.roll = 0.7853982F;
        rightMiddleLeg.roll = -0.58119464F;
        leftMiddleLeg.roll = 0.58119464F;
        rightMiddleFrontLeg.roll = -0.58119464F;
        leftMiddleFrontLeg.roll = 0.58119464F;
        rightFrontLeg.roll = -0.7853982F;
        leftFrontLeg.roll = 0.7853982F;
        rightHindLeg.yaw = 0.7853982F;
        leftHindLeg.yaw = -0.7853982F;
        rightMiddleLeg.yaw = 0.3926991F;
        leftMiddleLeg.yaw = -0.3926991F;
        rightMiddleFrontLeg.yaw = -0.3926991F;
        leftMiddleFrontLeg.yaw = 0.3926991F;
        rightFrontLeg.yaw = -0.7853982F;
        leftFrontLeg.yaw = 0.7853982F;
        float i = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 0.0F) * 0.4F) * limbDistance;
        float j = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 3.1415927F) * 0.4F) * limbDistance;
        float k = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 1.5707964F) * 0.4F) * limbDistance;
        float l = -(MathHelper.cos(limbAngle * 0.6662F * 2.0F + 4.712389F) * 0.4F) * limbDistance;
        float m = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 0.0F) * 0.4F) * limbDistance;
        float n = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 3.1415927F) * 0.4F) * limbDistance;
        float o = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 1.5707964F) * 0.4F) * limbDistance;
        float p = Math.abs(MathHelper.sin(limbAngle * 0.6662F + 4.712389F) * 0.4F) * limbDistance;
        ModelPart modelPart = rightHindLeg;
        modelPart.yaw += i;
        modelPart = leftHindLeg;
        modelPart.yaw += -i;
        modelPart = rightMiddleLeg;
        modelPart.yaw += j;
        modelPart = leftMiddleLeg;
        modelPart.yaw += -j;
        modelPart = rightMiddleFrontLeg;
        modelPart.yaw += k;
        modelPart = leftMiddleFrontLeg;
        modelPart.yaw += -k;
        modelPart = rightFrontLeg;
        modelPart.yaw += l;
        modelPart = leftFrontLeg;
        modelPart.yaw += -l;
        modelPart = rightHindLeg;
        modelPart.roll += m;
        modelPart = leftHindLeg;
        modelPart.roll += -m;
        modelPart = rightMiddleLeg;
        modelPart.roll += n;
        modelPart = leftMiddleLeg;
        modelPart.roll += -n;
        modelPart = rightMiddleFrontLeg;
        modelPart.roll += o;
        modelPart = leftMiddleFrontLeg;
        modelPart.roll += -o;
        modelPart = rightFrontLeg;
        modelPart.roll += p;
        modelPart = leftFrontLeg;
        modelPart.roll += -p;
    }
}
