/*
 * GreenLynxRenderer.java
 *  
 *  Copyright (c) 2020 Michael Sheppard
 *  
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.spidermod.client;


import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import net.spidermod.common.GreenLynxEntity;
import net.spidermod.common.SpiderMod;

public class GreenLynxRenderer<T extends GreenLynxEntity> extends MobEntityRenderer<T, GreenLynxModel<T>> {
    private static final Identifier TEXTURE = new Identifier(SpiderMod.MODID, "textures/entity/green_lynx/green_lynx.png");

    public GreenLynxRenderer(EntityRendererFactory.Context context) {
        super(context, new GreenLynxModel<>(context.getPart(SpiderModClient.greenLynxModelLayer)), 0.8F);
        addFeature(new GreenLynxEyesLayer<>(this));
        shadowRadius *= 0.15f;
        shadowOpacity *= 0.5f;
    }

    protected void scale(T entity, MatrixStack matrixStack, float partialTickTime) {
        float scale = entity.getScaleFactor();
        matrixStack.scale(scale, scale, scale);
        super.scale(entity, matrixStack, partialTickTime);
    }

    protected float getLyingAngle(T spiderEntity) {
        return 180.0F;
    }

    public Identifier getTexture(T spiderEntity) {
        return TEXTURE;
    }
}
